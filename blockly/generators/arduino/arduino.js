/**
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * http://code.google.com/p/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating Arduino for control blocks.
 * @author silver.kuusik@gmail.com (Silver Kuusik)
 */

Blockly.Arduino = Blockly.Generator.get('Arduino');

Blockly.Arduino.arduino_main = function() {
  
  var branch0 = Blockly.Arduino.statementToCode(this, 'SETUP');
  var branch1 = Blockly.Arduino.statementToCode(this, 'LOOP');
  
  var code;
  code = 'void setup() {\n' + branch0 + '}\n\n';
  code = code + 'void loop() {\n' + branch1 + '}\n';
  
  return code;
};

Blockly.Arduino.arduino_motor = function() {
  var code = 'start();\n';
  return code;
};

Blockly.Arduino.arduino_move = function() {
  var code = this.getTitleValue('MOVE') + '();\n';
  return code;
};

Blockly.Arduino.arduino_delay = function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'NUM',
      Blockly.Arduino.ORDER_NONE);
  return 'dealy(' + argument0 + ');\n';
};

Blockly.Arduino.arduino_opponent = function() {
  var code = this.getTitleValue('OPPONENT');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.arduino_line = function() {
  var code = this.getTitleValue('LINE');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};