/**
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * http://code.google.com/p/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating Arduino for control blocks.
 * @author silver.kuusik@gmail.com (Silver Kuusik)
 */

if (!Blockly.Language) Blockly.Language = {};
Blockly.LANG_CATEGORY_ARDUINO = "Arduino";
Blockly.LANG_ARDUINO_HELPURL = "No help";
Blockly.LANG_ARDUINO_TOOLTIP = "No tootltip";

Blockly.Language.arduino_main = {
  category: Blockly.LANG_CATEGORY_ARDUINO,
  helpUrl: Blockly.LANG_ARDUINO_HELPURL,
  init: function() {
    this.setColour(65);
    this.appendTitle("Arduino");
    this.appendInput(Blockly.NEXT_STATEMENT, 'SETUP')
        .appendTitle("Setup");
    this.appendInput(Blockly.NEXT_STATEMENT, 'LOOP')
        .appendTitle("Loop");
    this.setTooltip(Blockly.LANG_ARDUINO_TOOLTIP);
  }
};

Blockly.Language.arduino_motor = {
  category: Blockly.LANG_CATEGORY_ARDUINO,
  helpUrl: Blockly.LANG_ARDUINO_HELPURL,
  init: function() {
    this.setColour(330);
    this.appendTitle('Start motors');
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip(Blockly.LANG_ARDUINO_TOOLTIP);
  }
};

Blockly.Language.arduino_move = {
  category: Blockly.LANG_CATEGORY_ARDUINO,
  helpUrl: Blockly.LANG_ARDUINO_HELPURL,
  init: function() {
    this.setColour(330);
    var dropdown = new Blockly.FieldDropdown(this.OPERATORS);
    this.appendTitle(dropdown, 'MOVE');
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip(Blockly.LANG_ARDUINO_TOOLTIP);
  }
};

Blockly.Language.arduino_move.OPERATORS =
    [['Forward', 'forward'],
     ['Backward', 'backward'],
     ['Left', 'left'],
     ['Right', 'right']];

Blockly.Language.arduino_delay = {
  // Print statement.
  category: Blockly.LANG_CATEGORY_ARDUINO,
  helpUrl: Blockly.LANG_ARDUINO_HELPURL,
  init: function() {
    this.setColour(330);
    this.appendTitle('Delay');
    this.appendInput(Blockly.INPUT_VALUE, 'NUM', Number);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip(Blockly.LANG_TEXT_PRINT_TOOLTIP_1);
  }
};

Blockly.Language.arduino_opponent = {
  category: Blockly.LANG_CATEGORY_ARDUINO,
  helpUrl: Blockly.LANG_ARDUINO_HELPURL,
  init: function() {
    this.setColour(120);
    var dropdown = new Blockly.FieldDropdown(this.OPERATORS);
    this.appendTitle(dropdown, 'OPPONENT');
    this.setOutput(true, Boolean);
    this.setTooltip(Blockly.LANG_ARDUINO_TOOLTIP);
  }
};

Blockly.Language.arduino_opponent.OPERATORS =
    [['Opponent left', 'OPPONENT_LEFT'],
     ['Opponent front', 'OPPONENT_FRONT'],
     ['Opponent right', 'OPPONENT_RIGHT']];

Blockly.Language.arduino_line = {
  category: Blockly.LANG_CATEGORY_ARDUINO,
  helpUrl: Blockly.LANG_ARDUINO_HELPURL,
  init: function() {
    this.setColour(120);
    var dropdown = new Blockly.FieldDropdown(this.OPERATORS);
    this.appendTitle(dropdown, 'LINE');
    this.setOutput(true, Boolean);
    this.setTooltip(Blockly.LANG_ARDUINO_TOOLTIP);
  }
};

Blockly.Language.arduino_line.OPERATORS =
    [['Line left', 'BOTTOM_LEFT'],
     ['Line middle', 'BOTTOM_FRONT'],
     ['line right', 'BOTTOM_RIGHT']];