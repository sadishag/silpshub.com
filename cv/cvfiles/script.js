function showEN() {  
    document.getElementById("en").style.display = "block";
    document.getElementById("de").style.display = "none";
    document.getElementById("ee").style.display = "none";
    document.getElementById("jp").style.display = "none";
}

function showDE() {  
    document.getElementById("en").style.display = "none";
    document.getElementById("de").style.display = "block";
    document.getElementById("ee").style.display = "none";
    document.getElementById("jp").style.display = "none";
}

function showEE() { 
    document.getElementById("en").style.display = "none";
    document.getElementById("de").style.display = "none";
    document.getElementById("ee").style.display = "block";
    document.getElementById("jp").style.display = "none";
}

function showJP() { 
    document.getElementById("en").style.display = "none";
    document.getElementById("de").style.display = "none";
    document.getElementById("ee").style.display = "none";
    document.getElementById("jp").style.display = "block";
}